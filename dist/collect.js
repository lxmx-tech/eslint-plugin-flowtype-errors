'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _toArray2 = require('babel-runtime/helpers/toArray');

var _toArray3 = _interopRequireDefault(_toArray2);

exports.collect = collect;
exports.coverage = coverage;

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _child_process = require('child_process');

var _child_process2 = _interopRequireDefault(_child_process);

var _slash = require('slash');

var _slash2 = _interopRequireDefault(_slash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var flowBin = void 0;
/**
 * Run Flow and collect errors in JSON format
 *
 * Reference the following links for possible bug fixes and optimizations
 * https://github.com/facebook/nuclide/blob/master/pkg/nuclide-flow-rpc/lib/FlowRoot.js
 * https://github.com/ptmt/tryflow/blob/gh-pages/js/worker.js
 */


try {
  if (!process.env.FLOW_BIN) {
    flowBin = require('flow-bin'); // eslint-disable-line global-require
  }
} catch (e) {
  /* eslint-disable */
  console.log();
  console.log('Oops! Something went wrong! :(');
  console.log();
  console.log('eslint-plugin-flowtype-errors could not find the package "flow-bin". This can happen for a couple different reasons.');
  console.log();
  console.log('1. If ESLint is installed globally, then make sure "flow-bin" is also installed globally.');
  console.log();
  console.log('2. If ESLint is installed locally, then it\'s likely that "flow-bin" is not installed correctly. Try reinstalling by running the following:');
  console.log();
  console.log('  npm i -D flow-bin@latest');
  console.log();
  process.exit(1);
  /* eslint-enable */
}

// Adapted from https://github.com/facebook/flow/blob/master/tsrc/flowResult.js


function mainLocOfError(error) {
  var operation = error.operation,
      message = error.message;

  return operation && operation.loc || message[0].loc;
}

function fatalError(message) {
  return [{
    message,
    loc: { start: { line: 1, column: 1 }, end: { line: 1, column: 1 } }
  }];
}

function formatSeePath(message, root, flowVersion) {
  return message.loc && message.loc.type === 'LibFile' ? `https://github.com/facebook/flow/blob/v${flowVersion}/lib/${_path2.default.basename(message.path)}#L${message.line}` : `.${(0, _slash2.default)(message.path.replace(root, ''))}:${message.line}`;
}

function formatMessage(message, root, path, flowVersion) {
  var isOnlyMessage = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

  switch (message.type) {
    case 'Comment':
      return `${message.descr}`;
    case 'Blame':
      {
        var see = message.path !== '' ? ` See ${path === message.path ? `line ${message.line}` : formatSeePath(message, root, flowVersion)}.` : '';
        if (isOnlyMessage) {
          return `${message.descr}.${see}`;
        }
        // Omit duplicated message that should already be in `firstMessage` in the collect() function
        return message.descr.startsWith('property `') ? see.slice(1) : `'${message.descr}'.${see}`;
      }
    default:
      return `'${message.descr}'.`;
  }
}

function getFlowBin() {
  return process.env.FLOW_BIN || flowBin;
}

var didExecute = false;

function onExit(root) {
  if (!didExecute) {
    didExecute = true;
    process.on('exit', function () {
      return _child_process2.default.spawnSync(getFlowBin(), ['stop', root]);
    });
  }
}

function spawnFlow(mode, input, root, stopOnExit, filepath) {
  if (!input) {
    return true;
  }

  var child = _child_process2.default.spawnSync(getFlowBin(), [mode, '--json', `--root=${root}`, filepath], {
    input,
    encoding: 'utf-8'
  });

  var stdout = child.stdout;

  if (!stdout) {
    // Flow does not support 32 bit OS's at the moment.
    return false;
  }

  if (stopOnExit) {
    onExit(root);
  }

  return stdout.toString();
}

function determineRuleType(description) {
  return description.toLowerCase().includes('missing annotation') ? 'missing-annotation' : 'default';
}

function collect(stdin, root, stopOnExit, filepath) {
  var stdout = spawnFlow('check-contents', stdin, root, stopOnExit, filepath);

  if (typeof stdout !== 'string') {
    return stdout;
  }

  var json = void 0;

  try {
    json = JSON.parse(stdout);
  } catch (e) {
    return fatalError('Flow returned invalid json');
  }

  if (!Array.isArray(json.errors)) {
    return json.exit ? fatalError(`Flow returned an error: ${json.exit.msg} (code: ${json.exit.code})`) : fatalError('Flow returned invalid json');
  }

  var fullFilepath = _path2.default.resolve(root, filepath);

  // Loop through errors in the file
  var output = json.errors
  // Temporarily hide the 'inconsistent use of library definitions' issue
  .filter(function (error) {
    var mainLoc = mainLocOfError(error);
    var mainFile = mainLoc && mainLoc.source;
    return mainFile && error.message[0].descr && !error.message[0].descr.includes('inconsistent use of') && _path2.default.resolve(root, mainFile) === fullFilepath;
  }).map(function (error) {
    var message = error.message,
        operation = error.operation,
        extra = error.extra;


    var firstMessage = void 0;
    var remainingMessages = null;
    var mainErrorMessage = operation || message[0];

    if (extra !== undefined && extra.length > 0) {
      var _children = extra[0].children;
      var childMessages = _children !== undefined && _children.length > 0 ? _children[0].message : [];

      var _extra$0$message$conc = extra[0].message.concat(childMessages);

      var _extra$0$message$conc2 = (0, _toArray3.default)(_extra$0$message$conc);

      firstMessage = _extra$0$message$conc2[0];
      remainingMessages = _extra$0$message$conc2.slice(1);


      if (remainingMessages.length > 0 && remainingMessages[0].path === mainErrorMessage.path && remainingMessages[0].line >= mainErrorMessage.line && remainingMessages[0].endline <= mainErrorMessage.endline) {
        mainErrorMessage = remainingMessages[0];
      }
    } else {
      var _concat = [].concat(operation || [], message);

      var _concat2 = (0, _toArray3.default)(_concat);

      firstMessage = _concat2[0];
      remainingMessages = _concat2.slice(1);
    }

    var entireMessage = remainingMessages.length === 0 ? formatMessage(firstMessage, root, mainErrorMessage.path, json.flowVersion, true) : `${firstMessage.descr.replace(/:$/, '')}: ${remainingMessages.map(function (currentMessage) {
      return formatMessage(currentMessage, root, mainErrorMessage.path, json.flowVersion);
    }).join(' ')}`;

    var loc = mainErrorMessage.loc;
    var finalMessage = entireMessage.replace(/\.$/, '');

    return (0, _extends3.default)({}, process.env.DEBUG_FLOWTYPE_ERRRORS === 'true' ? json : {}, {
      type: determineRuleType(finalMessage),
      message: finalMessage,
      path: mainErrorMessage.path,
      start: loc && loc.start.line,
      end: loc && loc.end.line,
      loc
    });
  });

  return output;
}

function coverage(stdin, root, stopOnExit, filepath) {
  var stdout = spawnFlow('coverage', stdin, root, stopOnExit, filepath);

  if (typeof stdout !== 'string') {
    return stdout;
  }

  var expressions = void 0;

  try {
    expressions = JSON.parse(stdout).expressions;
  } catch (e) {
    return {
      coveredCount: 0,
      uncoveredCount: 0
    };
  }

  return {
    coveredCount: expressions.covered_count,
    uncoveredCount: expressions.uncovered_count
  };
}