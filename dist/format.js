'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

exports.default = filter;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This module is responsible for formatting flowtype errors to be more friendly and understandable
 * Formatting should be disabled by default
 * @TODO
 */

function fomatMessage(description) {
  if (description.toLowerCase().includes("' This type")) {
    return description.replace('This type', 'type');
  }

  return description;
}

function filter(messages) {
  return messages.map(function (e) {
    return (0, _extends3.default)({}, e, {
      message: fomatMessage(e.message)
    });
  });
}
module.exports = exports['default'];