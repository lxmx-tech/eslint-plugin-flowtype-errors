'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _collect = require('./collect');

var _parse = require('./parse');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var runOnAllFiles = void 0;

function hasFlowPragma(code) {
  return (/@flow/.test(code)
  );
}

function lookupFlowDir(context) {
  var root = process.cwd();
  var flowDirSetting = context.settings && context.settings['flowtype-errors'] && context.settings['flowtype-errors'].flowDir || '.';

  return _fs2.default.existsSync(_path2.default.join(root, flowDirSetting, '.flowconfig')) ? _path2.default.join(root, flowDirSetting) : root;
}

function stopOnExit(context) {
  return !!(context.settings && context.settings['flowtype-errors'] && context.settings['flowtype-errors'].stopOnExit);
}

function errorFlowCouldNotRun() {
  return {
    loc: 1,
    message: `Flow could not be run. Possible causes include:
  * Running on 32-bit OS (https://github.com/facebook/flow/issues/2262)
  * Recent glibc version not available (https://github.com/flowtype/flow-bin/issues/49)
  * FLOW_BIN environment variable ${process.env.FLOW_BIN ? 'set incorrectly' : 'not set'}
.`
  };
}

exports.default = {
  rules: {
    'enforce-min-coverage': function enforceMinCoverage(context) {
      return {
        Program() {
          var scripts = (0, _parse.getScripts)(context);

          var requiredCoverage = context.options[0];

          var _scripts$reduce = scripts.reduce(function (acc, script) {
            if (hasFlowPragma(script.code)) {
              var res = (0, _collect.coverage)(script.code, lookupFlowDir(context), stopOnExit(context), context.getFilename());

              if (res === true) {
                return acc;
              }

              if (res === false) {
                context.report(errorFlowCouldNotRun());
                return acc;
              }

              acc.coveredCount += res.coveredCount;
              acc.uncoveredCount += res.uncoveredCount;
            }

            return acc;
          }, { coveredCount: 0, uncoveredCount: 0 }),
              coveredCount = _scripts$reduce.coveredCount,
              uncoveredCount = _scripts$reduce.uncoveredCount;

          /* eslint prefer-template: 0 */


          var percentage = Number(Math.round(coveredCount / (coveredCount + uncoveredCount) * 10000) + 'e-2');

          if (percentage < requiredCoverage) {
            context.report({
              loc: 1,
              message: `Expected coverage to be at least ${requiredCoverage}%, but is: ${percentage}%`
            });
          }
        }
      };
    },
    'show-errors': function showErrors(context) {
      return {
        Program() {
          var scripts = (0, _parse.getScripts)(context);
          var flowDir = lookupFlowDir(context);

          // Check to see if we should run on every file
          if (runOnAllFiles === undefined) {
            try {
              runOnAllFiles = _fs2.default.readFileSync(_path2.default.join(flowDir, '.flowconfig')).toString().includes('all=true');
            } catch (err) {
              runOnAllFiles = false;
            }
          }

          scripts.forEach(function (script) {
            if (runOnAllFiles === false && !hasFlowPragma(script.code)) {
              return;
            }

            var collected = (0, _collect.collect)(script.code, flowDir, stopOnExit(context), context.getFilename());

            if (collected === true) {
              return;
            }

            if (collected === false) {
              context.report(errorFlowCouldNotRun());
              return;
            }

            collected.forEach(function (_ref) {
              var loc = _ref.loc,
                  message = _ref.message;

              context.report({
                loc: loc ? (0, _extends3.default)({}, loc, {
                  start: (0, _extends3.default)({}, loc.start, {
                    // Flow's column numbers are 1-based, while ESLint's are 0-based.
                    column: loc.start.column - 1,
                    row: loc.start.line + script.start
                  })
                }) : loc,
                message
              });
            });
          });
        }
      };
    }
  }
};
module.exports = exports['default'];