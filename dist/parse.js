'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Script = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

exports.getScripts = getScripts;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Script = exports.Script = function () {
  function Script(start) {
    var code = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    (0, _classCallCheck3.default)(this, Script);

    this.start = start;
    this.code = code;
  }

  (0, _createClass3.default)(Script, [{
    key: 'appendLine',
    value: function appendLine(line) {
      return new Script(this.start, [this.code, line].join(''));
    }
  }]);
  return Script;
}();

function parseCodeBlockLine(line, _ref) {
  var scripts = _ref.scripts,
      script = _ref.script;

  if (/<\/script>/.test(line)) {
    scripts.push(script);
    return { scripts };
  }

  return { scripts, script: script.appendLine(line) };
}

function parseHtmlBlockLine(line, idx, _ref2) {
  var scripts = _ref2.scripts;

  if (/<script>/.test(line)) {
    return { scripts, script: new Script(idx + 1) };
  }

  return { scripts };
}

function parseLine(state, line, idx) {
  if (state.script) {
    return parseCodeBlockLine(line, state);
  }

  return parseHtmlBlockLine(line, idx, state);
}

function extractHtmlScripts(code) {
  var lines = code.match(/[^\r\n]*[\r\n]+/g);

  var res = lines.reduce(parseLine, { scripts: [] });
  return res.scripts;
}

function isHtmlLike(context) {
  return (/(\.html)|(\.vue)$/.test(context.getFilename())
  );
}

function getScripts(context) {
  var code = context.getSourceCode().getText();

  if (isHtmlLike(context)) {
    return extractHtmlScripts(code);
  }

  return [new Script(0, code)];
}