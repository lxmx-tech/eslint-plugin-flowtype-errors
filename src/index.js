/* @flow */

import path from 'path';
import fs from 'fs';
import { collect, coverage } from './collect';
import { getScripts } from './parse';
import type { EslintContext } from './context';

let runOnAllFiles;

function hasFlowPragma(code): boolean {
  return /@flow/.test(code);
}

function lookupFlowDir(context: EslintContext): string {
  const root = process.cwd();
  const flowDirSetting: string =
    (context.settings &&
      context.settings['flowtype-errors'] &&
      context.settings['flowtype-errors'].flowDir) ||
    '.';

  return fs.existsSync(path.join(root, flowDirSetting, '.flowconfig'))
    ? path.join(root, flowDirSetting)
    : root;
}

function stopOnExit(context: EslintContext): boolean {
  return !!(
    context.settings &&
    context.settings['flowtype-errors'] &&
    context.settings['flowtype-errors'].stopOnExit
  );
}

function errorFlowCouldNotRun() {
  return {
    loc: 1,
    message:
`Flow could not be run. Possible causes include:
  * Running on 32-bit OS (https://github.com/facebook/flow/issues/2262)
  * Recent glibc version not available (https://github.com/flowtype/flow-bin/issues/49)
  * FLOW_BIN environment variable ${process.env.FLOW_BIN ? 'set incorrectly' : 'not set'}
.`
  };
}

export default {
  rules: {
    'enforce-min-coverage': function enforceMinCoverage(
      context: EslintContext
    ) {
      return {
        Program() {
          const scripts = getScripts(context);

          const requiredCoverage = context.options[0];
          const { coveredCount, uncoveredCount } = scripts.reduce((acc, script) => {
            if (hasFlowPragma(script.code)) {
              const res = coverage(
                script.code,
                lookupFlowDir(context),
                stopOnExit(context),
                context.getFilename()
              );

              if (res === true) {
                return acc;
              }

              if (res === false) {
                context.report(errorFlowCouldNotRun());
                return acc;
              }

              acc.coveredCount += res.coveredCount;
              acc.uncoveredCount += res.uncoveredCount;
            }

            return acc;
          }, { coveredCount: 0, uncoveredCount: 0 });

          /* eslint prefer-template: 0 */
          const percentage = Number(
            Math.round(
              coveredCount / (coveredCount + uncoveredCount) * 10000
            ) + 'e-2'
          );

          if (percentage < requiredCoverage) {
            context.report({
              loc: 1,
              message: `Expected coverage to be at least ${requiredCoverage}%, but is: ${percentage}%`
            });
          }
        }
      };
    },
    'show-errors': function showErrors(context: EslintContext) {
      return {
        Program() {
          const scripts = getScripts(context);
          const flowDir = lookupFlowDir(context);

          // Check to see if we should run on every file
          if (runOnAllFiles === undefined) {
            try {
              runOnAllFiles = fs
                .readFileSync(path.join(flowDir, '.flowconfig'))
                .toString()
                .includes('all=true');
            } catch (err) {
              runOnAllFiles = false;
            }
          }

          scripts.forEach(script => {
            if (runOnAllFiles === false && !hasFlowPragma(script.code)) {
              return;
            }

            const collected = collect(
              script.code,
              flowDir,
              stopOnExit(context),
              context.getFilename()
            );

            if (collected === true) {
              return;
            }

            if (collected === false) {
              context.report(errorFlowCouldNotRun());
              return;
            }

            collected.forEach(({ loc, message }) => {
              context.report({
                loc: loc
                  ? {
                      ...loc,
                      start: {
                        ...loc.start,
                        // Flow's column numbers are 1-based, while ESLint's are 0-based.
                        column: loc.start.column - 1,
                        row: loc.start.line + script.start
                      }
                    }
                  : loc,
                message
              });
            });
          });
        }
      };
    }
  }
};
